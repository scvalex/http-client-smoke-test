FROM alpine:3.11

RUN apk add --no-cache openssl ncurses-libs elixir

ARG APP_NAME

ENV REPLACE_OS_VARS=true
ENV APP_NAME=${APP_NAME}
ENV MIX_ENV=prod
ENV INSTALL_PATH=/app
ENV HOME=${INSTALL_PATH}

RUN mkdir -p ${INSTALL_PATH}
RUN chown nobody:nobody ${INSTALL_PATH}

WORKDIR ${INSTALL_PATH}

USER nobody:nobody

# COPY --chown=nobody:nobody _build/${MIX_ENV}/rel/${APP_NAME}/ "${INSTALL_PATH}/"
COPY --chown=nobody:nobody http_client_smoke_test "${INSTALL_PATH}"
COPY --chown=nobody:nobody priv/top_sites.csv "${INSTALL_PATH}"

ENTRYPOINT ["/bin/sh", "-c", "./http_client_smoke_test"]
