defmodule HttpClientSmokeTest do
  @moduledoc false

  def main(args \\ []) do
    args
    |> parse_args()
    |> run()
  end

  defp parse_args(args) do
    {_opts, _args, _invalid} =
      args
      |> OptionParser.parse(switches: [])

    :ok
  end

  defp run(_) do
    with {:ok, conn_pid} <- :gun.open('google.com', 443),
         {:ok, _protocol} <- :gun.await_up(conn_pid),
         stream_ref <- :gun.get(conn_pid, '/') do
      case :gun.await(conn_pid, stream_ref) do
        {:response, :fin, _status, _headers} ->
          IO.puts("No data")

        {:response, :nofin, _status, _headers} ->
          {:ok, body} = :gun.await_body(conn_pid, stream_ref)
          IO.puts(body)
      end
    end
  end
end
