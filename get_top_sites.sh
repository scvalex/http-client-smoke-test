#!/bin/bash

set -e -o pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

mkdir -p "${DIR}/priv"
curl "http://downloads.majestic.com/majestic_million.csv" | head -n 1001 | cut -d ',' -f 3 > "${DIR}/priv/top_sites.csv"
