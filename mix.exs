defmodule HttpClientSmokeTest.MixProject do
  use Mix.Project

  def project do
    [
      app: :http_client_smoke_test,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      escript: escript()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:gun, "~> 1.3"}
    ]
  end

  defp escript do
    [main_module: HttpClientSmokeTest]
  end
end
